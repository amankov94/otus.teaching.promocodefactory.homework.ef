﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Database.Configurations;

internal class CustomerConfiguration : IEntityTypeConfiguration<Customer>
{
    public void Configure(EntityTypeBuilder<Customer> builder)
    {
        builder.ToTable("customer");
        
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id)
            .HasColumnName("id");

        // builder.Property(x => x.Id)
        //     .ValueGeneratedNever();
        
        builder.Property(x => x.FirstName)
            .HasColumnName("first_name")
            .HasMaxLength(64)
            .IsRequired();
        
        builder.Property(x => x.LastName)
            .HasColumnName("last_name")
            .HasMaxLength(64)
            .IsRequired();
        
        builder.Property(x => x.Email)
            .HasColumnName("email")
            .HasMaxLength(320)
            .IsRequired();

        builder.Property(x => x.BirthDay)
            .HasColumnName("birth_day");

        builder.HasOne(x => x.PromoCode)
            .WithMany(x => x.Customers)
            .HasForeignKey("promo_code_id");

        builder.HasMany(x => x.Preferences)
            .WithMany(x => x.Customers)
            .UsingEntity<СustomerPreference>();

        builder.Ignore(x => x.FullName);
    }
}