﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Database.Configurations;

internal class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
{
    public void Configure(EntityTypeBuilder<Employee> builder)
    {
        builder.ToTable("employee");
        
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id)
            .HasColumnName("id");

        // builder.Property(x => x.Id)
        //     .ValueGeneratedNever();
        
        builder.Property(x => x.FirstName)
            .HasColumnName("first_name")
            .HasMaxLength(64)
            .IsRequired();
        
        builder.Property(x => x.LastName)
            .HasColumnName("last_name")
            .HasMaxLength(64)
            .IsRequired();
        
        builder.Property(x => x.Email)
            .HasColumnName("email")
            .HasMaxLength(320)
            .IsRequired();

        builder.Property(x => x.RoleId)
            .HasColumnName("role_id");

        builder.HasOne(x => x.Role)
            .WithMany(x => x.Employees)
            .HasForeignKey(x => x.RoleId);

        builder.Property(x => x.AppliedPromoCodesCount)
            .HasColumnName("applied_promo_codes_count");
    }
}