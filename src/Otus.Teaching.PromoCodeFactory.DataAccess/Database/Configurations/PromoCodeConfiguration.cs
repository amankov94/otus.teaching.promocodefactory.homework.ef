﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Database.Configurations;

public class PromoCodeConfiguration : IEntityTypeConfiguration<PromoCode>
{
    public void Configure(EntityTypeBuilder<PromoCode> builder)
    {
        builder.ToTable("promocode");

        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id)
            .HasColumnName("id");

        builder.Property(x => x.Code)
            .HasColumnName("code")
            .HasMaxLength(32)
            .IsRequired();

        builder.Property(x => x.PartnerName)
            .HasColumnName("partner_name")
            .HasMaxLength(32)
            .IsRequired();

        builder.Property(x => x.ServiceInfo)
            .HasColumnName("service_info")
            .HasMaxLength(32)
            .IsRequired();

        builder.Property(x => x.BeginDate)
            .HasColumnName("begin_date");
        
        builder.Property(x => x.EndDate)
            .HasColumnName("end_date");

        builder.HasOne(x => x.PartnerManager)
            .WithMany(x => x.PromoCodes)
            .HasForeignKey("employee_id");

        builder.HasOne(x => x.Preference)
            .WithMany(x => x.PromoCodes)
            .HasForeignKey("preference_id");
    }
}