﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Database.Configurations;

public class СustomerPreferenceConfiguration : IEntityTypeConfiguration<СustomerPreference>
{
    public void Configure(EntityTypeBuilder<СustomerPreference> builder)
    {
        builder.ToTable("customer_preference");
        builder.Property(x => x.CustomerId)
            .HasColumnName("customer_id");
        builder.Property(x => x.PreferenceId)
            .HasColumnName("preference_id");
    }
}