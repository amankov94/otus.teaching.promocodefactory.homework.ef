﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Database;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class EfRepository<T> : IRepository<T> where T : BaseEntity
{
    private readonly ApplicationDbContext _context;
    private readonly DbSet<T> _entities;

    public EfRepository(ApplicationDbContext context)
    {
        _context = context;
        _entities = _context.Set<T>();
    }

    public async Task<IEnumerable<T>> GetAllAsync()
    {
        return await _entities.ToListAsync();
    }

    public async Task<T> GetByIdAsync(Guid id)
    {
        return await _entities
            .FindAsync(id);
    }

    public async Task<T> GetFirstByFilter(Expression<Func<T, bool>> filter)
    {
        return await _entities.FirstOrDefaultAsync(filter);
    }

    public async Task<IEnumerable<T>> GetByFilter(Expression<Func<T, bool>> filter)
    {
        return await _entities.Where(filter).ToListAsync();
    }

    public async Task CreateAsync(T entity)
    {
        await _entities.AddAsync(entity);
        await _context.SaveChangesAsync();
    }

    public Task DeleteAsync(T entity)
    {
        _entities.Remove(entity);
        return _context.SaveChangesAsync();
    }

    public Task UpdateAsync(T entity)
    {
        _entities.Update(entity);
        return _context.SaveChangesAsync();
    }

    public IQueryable<T> Include(params Expression<Func<T, object>>[] includes)
    {
        IQueryable<T> query = _entities;
        foreach (var include in includes)
        {
            query = query.Include(include);
        }
        return query;
    }
}