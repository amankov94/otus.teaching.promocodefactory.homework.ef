﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

/// <summary>
/// Клиенты
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class CustomersController : ControllerBase
{
    private readonly IRepository<Customer> _customersRepository;
    private readonly IRepository<Preference> _preferenceRepository;

    public CustomersController(IRepository<Customer> customersRepository,
        IRepository<Preference> preferenceRepository)
    {
        _customersRepository = customersRepository;
        _preferenceRepository = preferenceRepository;
    }

    /// <summary>
    /// Получить данные всех клиентов
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
    {
        try
        {
            var customers = await _customersRepository.GetAllAsync();
            return customers.Select(customer => new CustomerShortResponse(customer)).ToList();
        }
        catch
        {
            return Problem();
        }
    }

    /// <summary>
    /// Получить данные клиента по id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
    {
        var customer = await _customersRepository
            .Include(x => x.Preferences, x => x.PromoCode)
            .FirstOrDefaultAsync(customer => customer.Id == id);
        
        if (customer == null)
            return NotFound();

        return new CustomerResponse
        {
            Id = customer.Id,
            FirstName = customer.FirstName,
            LastName = customer.LastName,
            Email = customer.Email,
            PromoCodes = customer.PromoCode != null
                ? new List<PromoCodeShortResponse>
                {
                    new(customer.PromoCode)
                }
                : null,
            Preferences = customer.Preferences?
                .Select(x => new PreferenceResponse() { Id = x.Id, Name = x.Name })
                .ToList()
        };
    }

    /// <summary>
    /// Создание нового клиента
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
    {
        await _customersRepository.CreateAsync(await CreateOrUpdateCustomer(null, request));
        return Ok();
    }

    /// <summary>
    /// Изменение данных клиента
    /// </summary>
    /// <param name="id"></param>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPut("{id:guid}")]
    public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
    {
        var customer = await _customersRepository.GetByIdAsync(id);
        if (customer == null)
            return NotFound();

        await _customersRepository.UpdateAsync(await CreateOrUpdateCustomer(customer, request));
        return Ok();
    }

    
    /// <summary>
    /// Удаление клиента
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpDelete]
    public async Task<IActionResult> DeleteCustomer(Guid id)
    {
        var customer = await _customersRepository.GetByIdAsync(id);
        if (customer == null)
            return NotFound();

        await _customersRepository.DeleteAsync(customer);
        return Ok();
    }

    private async Task<Customer> CreateOrUpdateCustomer(Customer customer, CreateOrEditCustomerRequest request)
    {
        customer ??= new Customer();
        var preferences = await _preferenceRepository.GetByFilter(entity => request.PreferenceIds.Contains(entity.Id));

        customer.LastName = request.LastName;
        customer.FirstName = request.FirstName;
        customer.Email = request.Email;
        customer.Preferences = preferences.ToList();

        return customer;
    }
}